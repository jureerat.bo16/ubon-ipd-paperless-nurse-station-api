import s from 'fluent-json-schema'

const paramsSchema = s.object()
    .prop('admit_id', s.string().format('uuid').required())

    export default {
        params: paramsSchema
    }