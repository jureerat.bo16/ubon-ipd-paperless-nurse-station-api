import S from 'fluent-json-schema'

const paramsSchema = S.object()
  .prop('an', S.string().maxLength(20))

export default {
  params: paramsSchema
}