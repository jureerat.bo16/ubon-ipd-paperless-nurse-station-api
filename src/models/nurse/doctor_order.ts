import { Knex } from 'knex';

export class DoctorOrderModel {

    getDoctorOrder(db: Knex, admit_id: any) {
        return db('admit')
        .leftJoin('doctor_order', 'doctor_order.admit_id', 'admit.id')
        .leftJoin('orders', 'orders.doctor_order_id', 'doctor_order.id',)
        .where('admit.id', admit_id);
      }

      insertDoctorOrder(db: Knex, dataDoctorOrder: any) {
        return db('doctor_order')
        .insert(dataDoctorOrder);
      }
      

}