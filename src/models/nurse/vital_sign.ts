import { Knex } from 'knex';

export class VitalSignModel {

    // Create by RENFIX
    insertVitalSign(db: Knex, data: any) {
        return db('vital_sign')
            .insert(data);
    }

    getVitalSign(db: Knex, admit_id: any) {
        return db('vital_sign')
            .leftJoin('admit', 'vital_sign.admit_id', 'admit.id')
            .where('vital_sign.admit_id', admit_id);
    }
}