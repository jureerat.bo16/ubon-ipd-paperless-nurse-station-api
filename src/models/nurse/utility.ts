import { Knex } from 'knex';
export class UtilityModel {

  //Create by RENFIX
  // transection เปลี่ยน ward/ update ตาราง admit เสร็จแล้วไป insert ที่ตารางpatient_move ต่อ
  changeWardTransaction(db: Knex, id: any, data: any, data2: any) {
    return new Promise((resolve: any, reject: any) => {
      db.transaction((trx) => {
        db('admit')
          .update(data)
          .where('id', id)
          .transacting(trx)
          .then(() => {
            // insert
            return db('patient_move')
              .insert(data2)
              .then(trx.commit)
              .catch(trx.rollback);
          })

      }).then(() => {
        resolve();
      })
        .catch((error: any) => {
          reject(error);
        })
    })
  }

  // Create By RENFIX
  // transection เปลี่ยนเตียง  update ตาราง admit เสร็จแล้วไป insert ที่ตาราง partient_move ต่อ
  assignBedTypeTransaction(db: Knex, id: any, data: any, data2: any,data_oldbed_id: any) {
    return new Promise((resolve: any, reject: any) => {
      db.transaction((trx) => {
        db('admit')
          .update(data)
          .where('id', id)
          .transacting(trx)
          .then(() => {
            // Uodate new bed
            return db('bed')
              .update('status','Used')
              .where('id', data_oldbed_id)
              .then(trx.commit)
              .catch(trx.rollback);
          })   .then(() => {
            // Uodate old bed to active
            return db('bed')
              .update('status','Used')
              .where('id', data_oldbed_id)
              .then(trx.commit)
              .catch(trx.rollback);
          })
          .then(() => {
            // insert
            return db('patient_move')
              .insert(data2)
              .then(trx.commit)
              .catch(trx.rollback);
          })

      }).then(() => {
        resolve();
      })
        .catch((error: any) => {
          reject(error);
        })
    })
  }


  // Create By RENFIX
  // transection เปลี่ยนเตียง  update ตาราง admit เสร็จแล้วไป insert ที่ตาราง partient_move ต่อ
  assignBedWithNoBed(db: Knex, id: any, bed_id: any) {
    return db('admit')
      .update('bed_id',bed_id)
      .where('id', id);
  }

    // ดึง bed_id เก่า จากตาราง Admit
  selectOldBed(db: Knex, id: any){
    return db('admit')
    .select('bed_id')
    .where('id', id);
  }

  assignWardWithNoWard(db: Knex, id: any, ward_id: any) {
    return db('admit')
      .update('ward_id',ward_id)
      .where('id', id);
  }


   // ดึง ward_id เก่า จากตาราง Admit
   selectOldWard(db: Knex, id: any){
    return db('admit')
    .select('ward_id')
    .where('id', id);
  }

}
