import { Knex } from 'knex';
import { UUID } from 'crypto';

export class NurseNoteModel {

    getNurseNote(db: Knex, admit_id: any) {
        return db('admit')
        .leftJoin('nurse_note', 'nurse_note.admit_id', 'admit.id')
        .where('admit.id', admit_id);
      }
      
      update(db: Knex, id: UUID, data: object) {
        let sql = db('nurse_note')
          .update(data)
          .where('id', id)
        return sql
      }

}