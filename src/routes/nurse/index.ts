import { FastifyInstance } from "fastify";

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  // verify jwt token
  fastify.addHook("onRequest", (request) => request.jwtVerify());

  fastify.register(require('./vital_sign'), { prefix: '/nurse/vital_sign' });
  fastify.register(require('./utility'), { prefix: '/nurse/utility' });
  //fastify.register(require('./assign_bed'), { prefix: '/assign_bed' });
  fastify.register(require('./activity'), { prefix: '/nurse/activity' });
  fastify.register(require('./admit'), { prefix: '/nurse/admit' });
  fastify.register(require('./patient'), { prefix: '/nurse/patient' });
  fastify.register(require('./doctor_order'), { prefix: '/nurse/doctor_order' });
  fastify.register(require('./nurse_note'), { prefix: '/nurse/nurse_note' });
  fastify.register(require('./info'), { prefix: '/nurse/info' });

  done();

} 
