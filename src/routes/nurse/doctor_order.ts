import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import _ from 'lodash';
//import { DateTime } from 'luxon';
import { DoctorOrderModel } from '../../models/nurse/doctor_order';
import viewNurseNote from '../../schema/nurse/view_nurse_note';


export default async (fastify: FastifyInstance, _options: any, done: any) => {

    const db = fastify.db;
    const getDoctorOrder = new DoctorOrderModel();

    // get patient
  fastify.get('/:admit_id', {
    preHandler: [
      fastify.guard.role(['nurse', 'admin','doctor']),
      // fastify.guard.scope('nurse.create', 'admit.create', 'nurse.read', 'admit.read')
    ],
    schema: viewNurseNote
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

      const params: any = request.params;
      const {admit_id} = params;

      const data = await getDoctorOrder.getDoctorOrder(db, admit_id);

      return reply.status(StatusCodes.CREATED).send({ ok: true,data });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }
  })

  done();

}

